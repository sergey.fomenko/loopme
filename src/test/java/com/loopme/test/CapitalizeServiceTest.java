package com.loopme.test;

import com.loopme.test.validation.ValidationInterceptor;
import io.grpc.ManagedChannel;
import io.grpc.ServerInterceptors;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.inprocess.InProcessChannelBuilder;
import io.grpc.inprocess.InProcessServerBuilder;
import io.grpc.testing.GrpcCleanupRule;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.IOException;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class CapitalizeServiceTest {

    public  final long DEADLINE_DURATION = 10L;
    private String positiveStr;
    private String negativeStr;
    private CapitalizeServiceGrpc.CapitalizeServiceBlockingStub blockingStub;
    private ManagedChannel channel;

    @Rule
    public final GrpcCleanupRule grpcCleanup = new GrpcCleanupRule();

    @Before
    public void setup() throws IOException {
        positiveStr = RandomStringUtils.randomAlphabetic(100);
        negativeStr = RandomStringUtils.randomAlphabetic(101);

        String serverName = InProcessServerBuilder.generateName();
        grpcCleanup.register(InProcessServerBuilder.forName(serverName)
                .directExecutor()
                .addService(ServerInterceptors.intercept(new CapitalizeServiceImpl(), new ValidationInterceptor()))
                .build()
                .start());

        channel = grpcCleanup.register(InProcessChannelBuilder.forName(serverName)
                .directExecutor()

                .build());

        blockingStub = CapitalizeServiceGrpc.newBlockingStub(channel);

        channel.getState(true);
    }

    @Test
    public void positiveCase(){
        CapitalizeRequest request = CapitalizeRequest.newBuilder()
                .setStr(positiveStr)
                .build();

        CapitalizeResponse response = blockingStub.capitalize(request);
        assertEquals(response.getStr(), positiveStr.toUpperCase());
    }

    @Test
    public void expectInvalidArgumentException(){
        CapitalizeRequest request = CapitalizeRequest.newBuilder()
                .setStr(negativeStr)
                .build();

        try {
            blockingStub.capitalize(request);
        } catch (StatusRuntimeException ex) {
            assertEquals(Status.INVALID_ARGUMENT.getCode(), ex.getStatus().getCode());
        }
    }

    @Test
    public void expectDeadlineAfter10Sec() {
        CapitalizeRequest request = CapitalizeRequest.newBuilder()
                .setStr(positiveStr)
                .build();
        try {
            channel.enterIdle();
            blockingStub.withDeadlineAfter(DEADLINE_DURATION, SECONDS).capitalize(request);
        } catch (StatusRuntimeException ex) {
            assertEquals(Status.DEADLINE_EXCEEDED.getCode(), ex.getStatus().getCode());
        }
    }
}