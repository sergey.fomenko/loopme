package com.loopme.test.validation;

public interface Validator {

    void validate(Object fieldValue, Object extensionValue) throws IllegalArgumentException;
}
