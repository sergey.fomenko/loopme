package com.loopme.test.validation;

import com.google.common.base.Preconditions;

public class MaxLengthValidator implements Validator {

    @Override
    public void validate(Object fieldValue, Object extensionValue){
        String text = "The length must be and less than 100 characters";
        if (fieldValue instanceof String) {
            Preconditions.checkArgument(fieldValue.toString().length() <= Integer.parseInt(extensionValue.toString()), text);
        }
    }

    @Override
    public String toString() {
        return "MaxLengthValidator";
    }
}
