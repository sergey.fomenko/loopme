package com.loopme.test.validation;

import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import io.grpc.ForwardingServerCallListener;
import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;

import java.util.Map;

public class ValidationInterceptor implements ServerInterceptor {

    @Override
    public <REQ, RES> ServerCall.Listener<REQ> interceptCall(ServerCall<REQ, RES> call,
                                                             Metadata headers,
                                                             ServerCallHandler<REQ, RES> next) {
        ServerCall.Listener<REQ> listener = next.startCall(call, headers);
        return new ForwardingServerCallListener.SimpleForwardingServerCallListener<REQ>(listener) {
            @Override
            public void onMessage(REQ message) {
                GeneratedMessageV3 messageV3 = (GeneratedMessageV3) message;
                try {
                    validate(messageV3);
                    super.onMessage(message);
                } catch (Exception e) {
                    Status status = Status.INVALID_ARGUMENT.withDescription(e.getMessage());
                    call.close(status, headers);
                    throw new StatusRuntimeException(status);
                }
            }
        };
    }

    protected void validate(GeneratedMessageV3 messageV3) {
        messageV3.getDescriptorForType().getFields()
                .forEach(fieldDescriptor -> {
                    if (messageV3.getField(fieldDescriptor) instanceof GeneratedMessageV3) {
                        GeneratedMessageV3 subMessageV3 = (GeneratedMessageV3) messageV3.getField(fieldDescriptor);
                        validate(subMessageV3);
                    } else if (fieldDescriptor.getOptions().getAllFields().size() > 0) {
                        fieldDescriptor.getOptions().getAllFields().entrySet()
                                .forEach(entry -> doValidate(messageV3, fieldDescriptor, entry));
                    }
                });
    }

    private void doValidate(GeneratedMessageV3 messageV3,
                            Descriptors.FieldDescriptor fieldDescriptor,
                            Map.Entry<Descriptors.FieldDescriptor, Object> entry) {
        Validators.getValidator(entry.getKey())
                .validate(messageV3.getField(fieldDescriptor), entry.getValue());
    }
}