package com.loopme.test.validation;

import com.google.common.collect.Maps;
import com.google.protobuf.Descriptors;
import validation.Validation;

import java.util.Map;

public class Validators {

    private static final Map<Descriptors.FieldDescriptor, Validator> VALIDATORS = Maps.newHashMap();

    static {
        VALIDATORS.put(Validation.maxLength.getDescriptor(), new MaxLengthValidator());
    }

    public static Validator getValidator(Descriptors.FieldDescriptor descriptor) {
        return VALIDATORS.get(descriptor);
    }
}