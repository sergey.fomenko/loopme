package com.loopme.test;

import io.grpc.stub.StreamObserver;

public class CapitalizeServiceImpl extends CapitalizeServiceGrpc.CapitalizeServiceImplBase {

    @Override
    public void capitalize(CapitalizeRequest request, StreamObserver<CapitalizeResponse> responseObserver) {

        CapitalizeResponse response = CapitalizeResponse.newBuilder()
                .setStr(request.getStr().toUpperCase())
                .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
